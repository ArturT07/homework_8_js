// 1 Завдання
const paragraphs = document.getElementsByTagName("p");
for (const paragraph of paragraphs) {
    paragraph.style.backgroundColor = "#ff0000";
}

// 2 Завдання
const optionsListElement = document.getElementById("optionsList");
console.log("Element with id 'optionsList':", optionsListElement);

const parentElement = optionsListElement.parentElement;
console.log("Parent element:", parentElement);

const childNodes = optionsListElement.childNodes;
console.log("Child nodes:");
for (const node of childNodes) {
    console.log("Node name:", node.nodeName, "Node type:", node.nodeType);
}

// 3 Завдання
const newParagraph = document.createElement('p');
newParagraph.textContent = 'This is a paragraph';
document.getElementById('testParagraph').appendChild(newParagraph);

// 4 Завдання
const mainHeader = document.querySelector('.main-header');
const nestedElements = mainHeader.querySelectorAll('*');
for (let i = 0; i < nestedElements.length; i++) {
    nestedElements[i].classList.add('nav-item');
    console.log(nestedElements[i]);
}

// 5 Завдання
const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach(title => {
    title.classList.remove('section-title');
});

